from game import Game
import pygame
import subprocess
import tkinter as INUIT
import sys
import os
from PIL import Image, ImageTk

# Prüffunktion ob Nummern im String vorhanden sind
def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)

# Aufrufen von Subprozessen die Versionsnummern abfragen
if not os.name == 'nt':
    subprocessjava = subprocess.Popen("java --version", shell=True, stdout=subprocess.PIPE)
    java = subprocessjava.stdout.read()

    subprocessnode = subprocess.Popen("node --version", shell=True, stdout=subprocess.PIPE)
    node = subprocessnode.stdout.read()

    subprocessnpm = subprocess.Popen("npm --version", shell=True, stdout=subprocess.PIPE)
    npm = subprocessnpm.stdout.read()
else:
    java = "v.1.0.14"
    node = "v.1.0.15"
    npm = "v.1.0.16"



# Abprüfen ob Versionsnummer existiert sonst Programm nicht installiert
if has_numbers(str(java)):
    javaInstalliert = True
else:
    javaInstalliert = False
if has_numbers(str(node)):
    nodeInstalliert = True
else:
    nodeInstalliert = False
if has_numbers(str(npm)):
    npmInstalliert = True
else:
    npmInstalliert = False

# Abfrage was passiert wenn vorausgesetzte Software installiert oder nicht installiert ist
if javaInstalliert and nodeInstalliert and npmInstalliert:
    gameStart = True
else:
    root = INUIT.Tk()
    root.title("INU.IT")
    root.focus_force()

    ico = Image.open('./Images/Inuit_Logo.png')
    photo = ImageTk.PhotoImage(ico)
    root.wm_iconphoto(False, photo)

    canvas1 = INUIT.Canvas(root, width=300, height=135)
    canvas1.pack()
    canvas1.configure(bg='black')

    def ExitApplication():
        root.destroy()
        sys.exit(0)

    def InstallRequirements():
        if not javaInstalliert:
            subprocess.call(['sh', './Scripts/installJava.sh'])
        if not nodeInstalliert:
            subprocess.call(['sh', './Scripts/installNodeJS.sh'])
        if not npmInstalliert:
            subprocess.call(['sh', './Scripts/installNPM.sh'])
        root.destroy()
        global gameStart
        gameStart = True


    Yesbutton = INUIT.Button(root, text='Installieren', command=InstallRequirements, bg='black', fg='white')
    NoButton = INUIT.Button(root, text='Beenden', command=ExitApplication, bg='black', fg='white')

    anzeigesoftware = "Vorausgesetzte Software ist nicht Installiert:"
    now = "Jetzt Installieren?"

    canvas1.create_text(150, 10, fill="white", text=anzeigesoftware)
    y = 30
    if not javaInstalliert:
        canvas1.create_text(125, y, fill="white", text="-Java")
        y = y + 20
    if not nodeInstalliert:
        canvas1.create_text(125, y, fill="white", text="-Node JS")
        y = y + 20
    if not npmInstalliert:
        canvas1.create_text(125, y, fill="white", text="-NPM")
        y = y + 20
    canvas1.create_text(125, y, fill="white", text=now)



    canvas1.create_window(90, y+30, window=Yesbutton)
    canvas1.create_window(200, y+30, window=NoButton)
    canvas1.config(width=300, height=y+50)
    root.geometry("+800+420")


    root.mainloop()


# Wenn vorausgesetzte Software installiert Starte Spiel
if gameStart:
    pygame.display.set_caption("INU.IT")
    programIcon = pygame.image.load("./Images/Inuit_Logo_32.png")
    pygame.display.set_icon(programIcon)

    g = Game()

    while g.running:
        pygame.display.set_caption('INU.IT')
        g.curr_menu.display_menu()
        g.game_loop()