# Remove Software
sudo rm -rf /home/pi/MagicMirror

# Remove Desktop Shortcut
sudo rm /home/pi/Desktop/MagicMirror.sh

# Deinstall Autostart
sudo rm /home/pi/MagicMirror/MagicMirror.sh

line=$(cat /etc/rc.local | grep -n "exit 0" | tail -1 | cut -f1 -d:)
insertline=$((line-1))
linecontains=$(sed "${insertline}q;d" /etc/rc.local)
if [ ! -z "$linecontains" ]
then
sudo sed -i "${insertline}s/.*//" /etc/rc.local
fi