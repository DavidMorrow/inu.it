# Copy Startscript to MagicMirror
cd Scripts
cp MagicMirror.sh /home/pi/MagicMirror

# Insert line into rc.local
line=$(cat /etc/rc.local | grep -n "exit 0" | tail -1 | cut -f1 -d:)
insertline=$((line-1))

linecontains=$(sed "${insertline}q;d" /etc/rc.local)

if [ -z "$linecontains" ]
then
sudo sed -i "${insertline}s/.*/su pi -c 'sh \/home\/pi\/MagicMirror\/MagicMirror.sh \&'/" /etc/rc.local
fi