import pygame
from menu import *
from PIL import Image

class Game():

    def __init__(self):
        pygame.init()

        self.xp = 20
        self.yp = 430
        self.vel = 10
        self.move_down = False
        self.move_up = False
        self.move_left = False
        self.move_right = False
        self.stepIndex = 0
        self.teleIndex = 0
        self.hebelaktiviert = False
        self.hebel2aktiviert = False
        self.hebel3aktiviert = False
        self.hebel4aktiviert = False
        self.speaker2 = False
        self.playerhitbox = (self.xp, self.yp,45,70)
        self.tilehitbox = (self.xp, self.yp,102,112)
        self.facing = ""
        self.level = False
        self.connected = False

        self.lefthit = False
        self.righthit = False
        self.uphit = False
        self.downhit = False

        self.newspaper = pygame.image.load("./Images/news.png")
        self.wolke = pygame.image.load("./Images/wolke.png")
        self.hebelL = pygame.image.load("./Images/hebelL.png")
        self.hebelR = pygame.image.load("./Images/hebelR.png")
        self.speaker = pygame.image.load("./Images/speaker.png")
        self.kalender = pygame.image.load("./Images/kalender.png")
        self.auto = pygame.image.load("./Images/autostart.png")
        self.restart = pygame.image.load("./Images/restart.png")
        self.start = pygame.image.load("./Images/startMM.png")
        self.verk = pygame.image.load("./Images/verk.png")
        self.lava = pygame.image.load("./Images/lava.png")
        self.wood = pygame.image.load("./Images/wood.png")
        self.walktile = pygame.image.load('./Images/icy.png')
        self.walltile = pygame.image.load('./Images/wall.png')
        self.doorO = pygame.image.load("./Images/door_opened.png")
        self.doorC = pygame.image.load("./Images/door_closed.png")
        self.doorO2 = pygame.image.load("./Images/door_opened2.png")
        self.doorC2 = pygame.image.load("./Images/door_closed2.png")

        self.stationary = pygame.image.load("./Images/Character/D1.png")
        self.lookdown = [pygame.image.load("./Images/Character/D1.png"),
                         pygame.image.load("./Images/Character/D2.png"),
                         pygame.image.load("./Images/Character/D3.png"),
                         pygame.image.load("./Images/Character/D4.png")]
        self.lookup = [pygame.image.load("./Images/Character/U1.png"),
                         pygame.image.load("./Images/Character/U2.png"),
                         pygame.image.load("./Images/Character/U3.png"),
                         pygame.image.load("./Images/Character/U4.png")]
        self.lookleft = [pygame.image.load("./Images/Character/L1.png"),
                         pygame.image.load("./Images/Character/L2.png"),
                         pygame.image.load("./Images/Character/L3.png"),
                         pygame.image.load("./Images/Character/L4.png")]
        self.lookright = [pygame.image.load("./Images/Character/R1.png"),
                         pygame.image.load("./Images/Character/R2.png"),
                         pygame.image.load("./Images/Character/R3.png"),
                         pygame.image.load("./Images/Character/R4.png")]
        self.tele = [pygame.image.load("./Images/Teleporter/T1.png"),
                       pygame.image.load("./Images/Teleporter/T2.png"),
                       pygame.image.load("./Images/Teleporter/T3.png"),
                       pygame.image.load("./Images/Teleporter/T4.png")]





        self.running, self.playing = True, False
        self.gameover = False
        self.deinstall = False
        self.shortcut = False
        self.restartPC = False
        self.autostartMM = False
        self.startMM = False
        self.ownweather = False
        self.owncalendar = False
        self.ownencouragement = False
        self.ownnews = False
        self.validinput = False
        self.anzahlEncourage = 0

        self.UP_KEY, self.DOWN_KEY, self.START_KEY, self.BACK_KEY = False, False, False, False
        self.DISPLAY_W, self.DISPLAY_H = 1020, 530
        self.display = pygame.Surface((self.DISPLAY_W, self.DISPLAY_H))

        self.window = pygame.display.set_mode(((self.DISPLAY_W, self.DISPLAY_H)))
        self.font_name = './Fonts/PressStart2p.ttf'
        self.BLACK, self.WHITE = (0, 0, 0), (255, 255, 255)
        self.GREY = (100, 100, 100)
        self.RED = (255, 0 ,0)
        self.main_menu = MainMenu(self)
        self.help = HelpMenu(self)
        self.credits = CreditsMenu(self)
        self.curr_menu = self.main_menu

    # Code orientiert sich an der Videoreihe https://www.youtube.com/watch?v=3a--b-QbEcw&list=PL30AETbxgR-feEfqwQxZ-_8s0fcvMQgqJ&index=1
    def draw_playerhitbox(self):
        self.playerhitbox = (self.xp+7, self.yp+10,34,62)
        #pygame.draw.rect(self.window,self.RED, self.playerhitbox,1)

    def draw_map(self,tiletype,posx,posy,string):
        self.window.blit(tiletype, (posx, posy))

        # Collision
        if string == "w":
         self.tilehitbox = (posx, posy,102,112)
         #pygame.draw.rect(self.window,self.RED, self.tilehitbox,2)
         recttile = pygame.Rect(posx,posy,102,100)
         rectplayer = pygame.Rect(self.xp+7,self.yp+10,34,62)
         collide = rectplayer.colliderect(recttile)
         if collide:
            if self.facing == "left":
                self.lefthit = True
                self.xp = self.xp + 10
            elif self.facing == "right":
                self.righthit = True
                self.xp = self.xp - 10
            elif self.facing == "up":
                self.uphit = True
                self.yp = self.yp + 10
            elif self.facing == "down":
                self.downhit = True
                self.yp = self.yp - 10
        if string == "n":
            self.newspaperhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.newspaperhitbox, 2)
            rectnewspaper = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectnewspaper)
            if collide:
                self.ownnews = True
        if string == "m":
            self.autohitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.autohitbox, 2)
            rectauto = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectauto)
            if collide:
                self.autostartMM = True
        if string == "ve":
            self.autohitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.autohitbox, 2)
            rectauto = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectauto)
            if collide:
                self.shortcut = True
        if string == "re":
            self.restarthitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.restarthitbox, 2)
            rectrestart = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectrestart)
            if collide:
                self.restartPC = True
        if string == "we":
            self.weatherhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.weatherhitbox, 2)
            rectweather = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectweather)
            if collide:
                self.ownweather = True
        if string == "k":
            self.kalenderhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.kalenderhitbox, 2)
            rectkalender = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectkalender)
            if collide:
                self.owncalendar = True
        if string == "d":
         self.doorhitbox = (posx+90, posy,15,112)
         #pygame.draw.rect(self.window,self.RED, self.doorhitbox,3)

         rectdoor = pygame.Rect(posx,posy,102,100)
         rectplayer = pygame.Rect(self.xp+7,self.yp+10,34,62)
         collide = rectplayer.colliderect(rectdoor)
         if collide:
            if self.facing == "left":
                self.lefthit = True
                self.xp = self.xp + 10
            elif self.facing == "right":
                self.righthit = True
                self.xp = self.xp - 10
            elif self.facing == "up":
                self.uphit = True
                self.yp = self.yp + 10
            elif self.facing == "down":
                self.downhit = True
                self.yp = self.yp - 10
        if string == "d2":
         self.doorhitbox = (posx, posy+96,112,15)
         #pygame.draw.rect(self.window,self.RED, self.doorhitbox,3)

         rectdoor = pygame.Rect(posx,posy,102,100)
         rectplayer = pygame.Rect(self.xp+7,self.yp+10,34,62)
         collide = rectplayer.colliderect(rectdoor)
         if collide:
            if self.facing == "left":
                self.lefthit = True
                self.xp = self.xp + 10
            elif self.facing == "right":
                self.righthit = True
                self.xp = self.xp - 10
            elif self.facing == "up":
                self.uphit = True
                self.yp = self.yp + 10
            elif self.facing == "down":
                self.downhit = True
                self.yp = self.yp - 10
        if string == "d3":
            self.doorhitbox = (posx + 90, posy, 15, 112)
            #pygame.draw.rect(self.window, self.RED, self.doorhitbox, 3)

            rectdoor = pygame.Rect(posx, posy, 102, 100)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectdoor)
            if collide:
                if self.facing == "left":
                    self.lefthit = True
                    self.xp = self.xp + 10
                elif self.facing == "right":
                    self.righthit = True
                    self.xp = self.xp - 10
                elif self.facing == "up":
                    self.uphit = True
                    self.yp = self.yp + 10
                elif self.facing == "down":
                    self.downhit = True
                    self.yp = self.yp - 10
        if string == "h":
            self.hebelhitbox = (posx+10, posy+10, 30, 30)
           #pygame.draw.rect(self.window, self.RED, self.hebelhitbox, 2)
            recthebel = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(recthebel)
            if collide:
                self.hebelaktiviert = True
        if string == "h2":
            self.hebelhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.hebelhitbox, 2)
            recthebel2 = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(recthebel2)
            if collide:
                self.hebel2aktiviert = True
        if string == "h3":
            self.hebelhitbox = (posx+10, posy+10, 30, 30)
           #pygame.draw.rect(self.window, self.RED, self.hebelhitbox, 2)
            recthebel2 = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(recthebel2)
            if collide:
                self.hebel3aktiviert = True
        if string == "h4":
            self.hebelhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.hebelhitbox, 2)
            recthebel2 = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(recthebel2)
            if collide:
                self.hebel4aktiviert = True
        if string == "s":
            self.speakerhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.speakerhitbox, 2)
            rectspeaker = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectspeaker)
            if collide:
                self.ownencouragement = True
                self.anzahlEncourage += 1
        if string == "s2":
            self.speakerhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.speakerhitbox, 2)
            rectspeaker = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectspeaker)
            if collide:
                self.ownencouragement = True
                self.speaker2 = True
                self.anzahlEncourage += 1
        if string == "l":
            self.lavahitbox = (posx, posy, 102, 112)
            #pygame.draw.rect(self.window, self.RED, self.lavahitbox, 2)
            rectlava = pygame.Rect(posx, posy, 102, 100)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectlava)
            if collide:
                self.level = False
                self.xp = 20
                self.yp = 430
        if string == "smm":
            self.smmhitbox = (posx+10, posy+10, 30, 30)
            #pygame.draw.rect(self.window, self.RED, self.smmhitbox, 2)
            rectsmm = pygame.Rect(posx, posy, 30, 30)
            rectplayer = pygame.Rect(self.xp + 7, self.yp + 10, 34, 62)
            collide = rectplayer.colliderect(rectsmm)
            if collide:
                self.startMM = True

    def draw_teleporter(self,posx,posy,loc):
        self.telehitbox = (posx+11, posy+20, 30, 40)
        if self.teleIndex >= 3:
            self.teleIndex = 0
        self.window.blit(self.tele[self.teleIndex], (posx, posy))
        #pygame.draw.rect(self.window, self.RED, self.telehitbox, 2)

        rectplayer = pygame.Rect(self.xp+7,self.yp+10,34,62)
        recttele = pygame.Rect(posx+11,posy+20,30,40)
        collide = rectplayer.colliderect(recttele)
        if collide:
            if loc == "topleft":
                self.level = True
                self.yp = 250
                self.xp = 950
            elif loc == "downright":
                self.level = True
                self.yp = 250
                self.xp = 10
            elif loc == "level2":
                self.level = False
                self.yp = 450
                self.xp = 900
            elif loc == "final":
                self.level = False
                self.yp = 450
                self.xp = 20

    def draw_player(self):
        if self.stepIndex >= 4:
            self.stepIndex = 0
        if self.move_down == True:
            self.window.blit(self.lookdown[self.stepIndex], (self.xp, self.yp))
            self.stepIndex += 1
        elif self.move_up == True:
            self.window.blit(self.lookup[self.stepIndex], (self.xp, self.yp))
            self.stepIndex += 1
        elif self.move_left == True:
            self.window.blit(self.lookleft[self.stepIndex], (self.xp, self.yp))
            self.stepIndex += 1
        elif self.move_right == True:
            self.window.blit(self.lookright[self.stepIndex], (self.xp, self.yp))
            self.stepIndex += 1
        else:
          self.window.blit(self.stationary, (self.xp, self.yp))

    def game_loop(self):

        start_ticks = pygame.time.get_ticks()

        while self.playing:

            self.check_events()

            #Gametimer Code von https://stackoverflow.com/questions/30720665/countdown-timer-in-pygame
            seconds = (pygame.time.get_ticks() - start_ticks) / 1000  # calculate how many seconds
            timer = 120 - seconds

            if timer > 60:
                if timer > 70:
                    telltime = "01" + ":" + str(int(timer)-60)
                else:
                    telltime = "01" + ":0" + str(int(timer)-60)
            else:
                if timer > 10:
                    telltime = "00" + ":" + str(int(timer))
                else:
                    telltime = "00" + ":0" + str(int(timer))

            # Endparameter
            if timer < 1:
                self.gameover = True
                self.playing = False

            # Dev Button
            #if self.START_KEY:
            #   self.level = True
            #    self.gameover = True
            #   self.playing = False
            #    self.ownencouragement = True
            #    self.anzahlEncourage = 2


            #Level 1
            if not self.level:
                x = 0
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")

                x=102
                self.draw_map(self.walltile,x,0,"w")
                if not self.hebelaktiviert:
                    self.draw_map(self.doorC,x,106,"d")
                else:
                    self.draw_map(self.doorO,x,106,"")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walltile,x,420,"w")

                x=204
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")

                x=306
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walltile,x,420,"w")

                x=408
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")

                x=510
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walltile,x,420,"w")

                x=612
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")

                x=714
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walltile,x,420,"w")

                x=816
                self.draw_map(self.walltile,x,0,"w")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walktile,x,420,"")

                x=918
                self.draw_map(self.walktile,x,0,"")
                if not self.hebel2aktiviert:
                    self.draw_map(self.doorC2,x,106,"d2")
                else:
                    self.draw_map(self.doorO2,x,106,"")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walktile,x,420,"")

                if not self.ownnews:
                    self.draw_map(self.newspaper, 840, 450, "n")
                if not self.ownweather:
                    self.draw_map(self.wolke, 240, 450, "we")
                if not self.ownencouragement and self.anzahlEncourage == 0:
                    self.draw_map(self.speaker, 435, 450, "s")
                if not self.shortcut:
                    self.draw_map(self.verk, 435, 250, "ve")

                if not self.hebelaktiviert:
                    self.draw_map(self.hebelL, 940, 56, "h")
                else:
                    self.draw_map(self.hebelR, 940, 56, "")

                if not self.hebel2aktiviert:
                    self.draw_map(self.hebelL, 640, 480, "h2")
                else:
                    self.draw_map(self.hebelR, 640, 480, "")

                self.draw_teleporter(23, 10, "topleft")
                self.draw_teleporter(940, 440, "downright")

            #Level 2
            else:
                x = 0
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")

                if not self.hebel3aktiviert:
                    self.draw_map(self.lava,x,216,"l")
                else:
                    self.draw_map(self.wood,x,216,"")

                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walltile,x,420,"w")

                x=102
                self.draw_map(self.walltile,x,0,"w")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walltile,x,420,"w")

                x=204
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")


                x=306
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walktile,x,420,"")

                x=408
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")

                x=510
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walltile,x,324,"w")
                self.draw_map(self.walltile,x,420,"w")

                x=612
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")

                x=714
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walltile,x,420,"w")

                x=816
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walltile,x,106,"w")
                self.draw_map(self.walltile,x,216,"w")
                self.draw_map(self.walltile,x,324,"w")
                if not self.hebel4aktiviert:
                    self.draw_map(self.doorC,x,420,"d3")
                else:
                    self.draw_map(self.doorO,x,420,"")

                x=918
                self.draw_map(self.walktile,x,0,"")
                self.draw_map(self.walktile,x,106,"")
                self.draw_map(self.walktile,x,216,"")
                self.draw_map(self.walktile,x,324,"")
                self.draw_map(self.walktile,x,420,"")


                if not self.hebel3aktiviert:
                    self.draw_map(self.hebelL, 830, 480, "h3")
                else:
                    self.draw_map(self.hebelR, 830, 480, "")
                    self.draw_teleporter(635, 440, "final")


                if not self.hebel4aktiviert:
                    self.draw_map(self.hebelL, 230, 480, "h4")
                else:
                    self.draw_map(self.hebelR, 230, 480, "")

                if not self.owncalendar:
                    self.draw_map(self.kalender, 20, 20, "k")



                if not self.autostartMM:
                    self.draw_map(self.auto, 734, 344, "m")

                if not self.restartPC and not self.startMM:
                    self.draw_map(self.restart, 229, 350, "re")

                if not self.startMM and not self.restartPC:
                    self.draw_map(self.start, 326, 236, "smm")

                if not self.speaker2:
                    self.draw_map(self.speaker, 540, 30, "s2")
                self.draw_teleporter(20, 330, "level2")


            #Clock Rate
            self.teleIndex += 1

            self.draw_player()
            self.draw_playerhitbox()

            #handling der User Eingaben
            userInput = pygame.key.get_pressed()
            if userInput[pygame.K_LEFT] and self.xp > 20:
                if not self.lefthit:
                    self.xp -= self.vel
                self.move_down = False
                self.move_up = False
                self.move_left = True
                self.move_right = False
                self.facing = "left"
                self.righthit = False
                self.uphit = False
                self.downhit = False
            elif userInput[pygame.K_RIGHT] and self.xp < 950:
                if not self.righthit:
                    self.xp += self.vel
                self.move_down = False
                self.move_up = False
                self.move_left = False
                self.move_right = True
                self.facing = "right"
                self.lefthit = False
                self.uphit = False
                self.downhit = False
            elif userInput[pygame.K_UP] and self.yp > 0:
                if not self.uphit:
                    self.yp -= self.vel
                self.move_down = False
                self.move_up = True
                self.move_left = False
                self.move_right = False
                self.facing = "up"
                self.lefthit = False
                self.righthit = False
                self.downhit = False
            elif userInput[pygame.K_DOWN] and self.yp < 450:
                if not self.downhit:
                    self.yp += self.vel
                self.move_down = True
                self.move_up = False
                self.move_left = False
                self.move_right = False
                self.facing = "down"
                self.lefthit = False
                self.lefthit = False
                self.uphit = False
                self.righthit = False
            else:
                self.move_down = False
                self.move_up = False
                self.move_left = False
                self.move_right = False
                self.stepIndex = 0


            #Ausgabe der Gewählten Funktionen
            #print("News: " + str(self.ownnews) + " " + "Weather: " + str(self.ownweather) + " " + "Encouragement: " + str(self.ownencouragement) + " " + str(self.anzahlEncourage) + " " + "Kalender: " + str(self.owncalendar) )
            #print("Autostart: " + str(self.autostartMM) + " " + "Restart: " + str(self.restartPC) + " " + "Verknüpfung: " + str(self.shortcut) + " " + "StartMM: " + str(self.startMM))

            #Textdarstellung des Timers
            font = pygame.font.Font('./Fonts/PressStart2p.ttf', 32)
            text = font.render(telltime, True, self.WHITE)
            textRect = text.get_rect()
            textRect.center = (self.window.get_width() // 2, 100 // 2)
            self.window.blit(text, textRect)

            pygame.display.update()
            self.reset_keys()


            # Verzögerung der Gameframes
            if os.name == 'nt':  # Windows Dev
                pygame.time.delay(20)
            else:
                pygame.time.delay(10)

    def check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running, self.playing = False, False
                self.curr_menu.run_display = False


            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    self.START_KEY = True
                if event.key == pygame.K_BACKSPACE:
                    self.BACK_KEY = True
                if event.key == pygame.K_DOWN:
                    self.DOWN_KEY = True
                if event.key == pygame.K_UP:
                    self.UP_KEY = True

    def reset_keys(self):
        self.UP_KEY, self.DOWN_KEY, self.START_KEY, self.BACK_KEY = False, False, False, False

    def draw_text(self, text, size, x, y):
        font = pygame.font.Font(self.font_name, size)

        if text == 'Deinstall':
            if self.deinstall:
                text_surface = font.render(text, True, self.BLACK)
            else:
                text_surface = font.render(text, True, self.GREY)
        elif text == 'Insert Data':
            if not self.ownweather and not self.owncalendar and not self.ownencouragement and not self.ownnews:
                text_surface = font.render(text, True, self.GREY)
            else:
                text_surface = font.render(text, True, self.BLACK)

        elif text == 'Finalise':
            if not self.validinput:
                text_surface = font.render(text, True, self.GREY)
            else:
                text_surface = font.render(text, True, self.BLACK)

        elif text == 'Play Install':
            if not self.connected:
                text_surface = font.render(text, True, self.GREY)
            else:
                text_surface = font.render(text, True, self.BLACK)

        else:
                text_surface = font.render(text, True, self.BLACK)

        text_rect = text_surface.get_rect()
        text_rect.center = (x, y)
        self.display.blit(text_surface, text_rect)