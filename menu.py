import os
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (960/2,540/2)
import urllib.request
from shutil import copyfile
import pygame
import subprocess
import os.path
from tkinter import messagebox
import tkinter as INUIT
import webbrowser
from tkinter import *
from PIL import Image, ImageTk
x = 100
y = 100

# Aufbau und programmierung des Menüs von https://www.youtube.com/watch?v=a5JWrd7Y_14 mit eigenen Anpassungen

class Menu():

    global insert_list

    def __init__(self, game):
        self.game = game
        self.mid_w, self.mid_h = self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2
        self.run_display = True
        self.cursor_rect = pygame.Rect(0, 0, 20, 20)
        self.offset = - 100
    def draw_cursor(self):
        self.game.draw_text('*', 15, self.cursor_rect.x - 25, self.cursor_rect.y)
    def blit_screen(self):
        self.game.window.blit(self.game.display, (0, 0))
        pygame.display.update()
        self.game.reset_keys()

def callback(url):
    webbrowser.open_new(url)
def changeline(line,ins):
    # Code von https://www.kite.com/python/answers/how-to-edit-a-specific-line-in-a-text-file-in-python
    file = open("./Config/config.js", "r")
    list_of_lines = file.readlines()
    list_of_lines[line-1] = ins

    file = open("./Config/config.js", "w")
    file.writelines(list_of_lines)
    file.close()
def save_input(self,e,INUIT):

    leercounter = 0
    iterate = 0
    global insert_list


    for x in e:
      if x != 0:
          input = x.get()
          if len(input) == 0:
              leercounter = leercounter + 1

    if leercounter == 0:
        for a in e:
            if a == 0:
                insert_list[iterate] = 0
            else:
                insert_list[iterate] = a.get()
            iterate = iterate + 1
        ExitPopup(INUIT)
        self.game.validinput = True
    else:
        messagebox.showerror('Error Inserting Data', 'Please fill every empty field')

def connect(host='http://google.com'):
    try:
        urllib.request.urlopen(host)
        return True
    except:
        return False

def ExitPopup(INUIT):
    INUIT.destroy()


class MainMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
        self.state = "Start"
        self.startx, self.starty = self.mid_w, self.mid_h + 40
        self.deinstx, self.deinsty = self.mid_w, self.mid_h + 70
        self.helpx, self.helpy = self.mid_w, self.mid_h + 100
        self.creditsx, self.creditsy = self.mid_w, self.mid_h + 130
        self.quitx, self.quity = self.mid_w, self.mid_h + 160
        self.cursor_rect.midtop = (self.startx + self.offset, self.starty)
        self.backgroundMain = pygame.image.load("./Images/bgmain.jpg")
        self.backgroundMainEND = pygame.image.load("./Images/bgmainEND.jpg")
        self.arrow = pygame.image.load("./Images/arrow.png")

    def display_menu(self):
        self.run_display = True

        # Prüfen ob installation schon existiert
        if os.path.isfile("/home/pi/MagicMirror/config/config.js"):
            self.game.deinstall = True
        else:
            self.game.deinstall = False

        #Display Menü
        if self.game.gameover == False:
            self.state = "Start"
            self.cursor_rect.midtop = (self.startx + self.offset, self.starty)
            while self.run_display:

                if connect():
                    self.game.connected = True
                else:
                    self.game.connected = False

                self.game.check_events()
                self.check_input()

                self.game.display.fill(self.game.BLACK)
                self.game.display.blit(self.backgroundMain, (0, 0))

                self.game.draw_text('Main Menu', 30, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 - 20)
                self.game.draw_text("Play Install", 20, self.startx, self.starty)
                self.game.draw_text("Deinstall", 20, self.deinstx, self.deinsty)
                self.game.draw_text("Help", 20, self.helpx, self.helpy)
                self.game.draw_text("Credits", 20, self.creditsx, self.creditsy)
                self.game.draw_text("Quit", 20, self.quitx, self.quity)
                self.draw_cursor()
                self.blit_screen()

        elif self.game.gameover:
            self.state = "APII"

            if not self.game.ownnews and not self.game.owncalendar and not self.game.ownweather and not self.game.ownencouragement:
                self.game.validinput = True
            self.cursor_rect.midtop = (self.deinstx + self.offset, self.deinsty)
            while self.run_display:
                self.game.check_events()
                self.check_input()
                self.game.display.fill(self.game.BLACK)
                self.game.display.blit(self.backgroundMainEND, (0, 0))

                if (self.game.ownnews or self.game.owncalendar or self.game.ownweather or self.game.ownencouragement) and not self.game.validinput:
                    self.game.display.blit(self.arrow, (110, 110))

                self.game.draw_text("Game Over", 30, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 - 20)
                self.game.draw_text("Insert Data", 20, self.deinstx, self.deinsty)
                self.game.draw_text("Play Again", 20, self.helpx, self.helpy)
                self.game.draw_text("Finalise", 20, self.creditsx, self.creditsy)
                self.game.draw_text("Main Menu", 20, self.creditsx, self.creditsy + 30)
                self.game.draw_text("Quit", 20, self.quitx, self.quity + 30)

                basicy = 70
                fs = 13
                if self.game.shortcut or self.game.restartPC or self.game.autostartMM or self.game.startMM:
                    self.game.draw_text("Additional Configuration", fs, 850, 30)
                    self.game.draw_text("---------------", fs, 850, 50)
                if self.game.restartPC:
                    self.game.draw_text("Restart Device", fs, 850, basicy)
                    basicy = basicy + 20
                if self.game.autostartMM:
                    self.game.draw_text("Autostart MagicMirror", fs, 850, basicy)
                    basicy = basicy + 20
                if self.game.shortcut:
                    self.game.draw_text("Create Shortcut", fs, 850, basicy)
                    basicy = basicy + 20
                if self.game.startMM:
                    self.game.draw_text("Start MagicMirror", fs, 850, basicy)
                    basicy = basicy + 20

                self.game.draw_text("Main Configuration", fs, 160, 30)
                self.game.draw_text("---------------", fs, 160, 50)
                if self.game.ownweather:
                    self.game.draw_text("Own Weather", fs, 160, 70)
                else:
                    self.game.draw_text("New York Weather", fs, 160, 70)
                if self.game.owncalendar:
                    self.game.draw_text("Own Calendar", fs, 160, 90)
                else:
                    self.game.draw_text("US-Holidays Calendar", fs, 160, 90)
                if self.game.ownencouragement:
                    self.game.draw_text("Own Encouragement", fs, 160, 110)
                else:
                    self.game.draw_text("Standard Encouragement", fs, 160, 110)
                if self.game.ownnews:
                    self.game.draw_text("Own Newsfeed", fs, 160, 130)
                else:
                    self.game.draw_text("New York Times Newsfeed", fs, 160, 130)

                self.draw_cursor()
                self.blit_screen()

    def move_cursor(self):
        if self.game.DOWN_KEY:
            if self.state == 'Start':
                self.cursor_rect.midtop = (self.deinstx + self.offset, self.deinsty)
                self.state = 'Deinst'
            elif self.state == 'Deinst':
                self.cursor_rect.midtop = (self.helpx + self.offset, self.helpy)
                self.state = 'Help'
            elif self.state == 'Help':
                self.cursor_rect.midtop = (self.creditsx + self.offset, self.creditsy)
                self.state = 'Credits'
            elif self.state == 'Credits':
                self.cursor_rect.midtop = (self.quitx + self.offset, self.quity)
                self.state = 'Quit'
            elif self.state == 'Quit':
                self.cursor_rect.midtop = (self.startx + self.offset, self.starty)
                self.state = 'Start'

            if self.state == 'APII':
                self.cursor_rect.midtop = (self.helpx + self.offset, self.helpy)
                self.state = 'Again'
            elif self.state == 'Again':
                self.cursor_rect.midtop = (self.creditsx + self.offset, self.creditsy)
                self.state = 'Finalise'
            elif self.state == 'Finalise':
                self.cursor_rect.midtop = (self.quitx + self.offset, self.quity)
                self.state = 'MM'
            elif self.state == 'MM':
                self.cursor_rect.midtop = (self.quitx + self.offset, self.quity + 30)
                self.state = 'Close'
            elif self.state == 'Close':
                self.cursor_rect.midtop = (self.deinstx + self.offset, self.deinsty)
                self.state = 'APII'
        elif self.game.UP_KEY:
            if self.state == 'Start':
                self.cursor_rect.midtop = (self.quitx + self.offset, self.quity)
                self.state = 'Quit'
            elif self.state == 'Help':
                self.cursor_rect.midtop = (self.deinstx + self.offset, self.deinsty)
                self.state = 'Deinst'
            elif self.state == 'Deinst':
                self.cursor_rect.midtop = (self.startx + self.offset, self.starty)
                self.state = 'Start'
            elif self.state == 'Credits':
                self.cursor_rect.midtop = (self.helpx + self.offset, self.helpy)
                self.state = 'Help'
            elif self.state == 'Quit':
                self.cursor_rect.midtop = (self.creditsx + self.offset, self.creditsy)
                self.state = 'Credits'

            if self.state == 'APII':
                self.cursor_rect.midtop = (self.quitx + self.offset, self.quity +20)
                self.state = 'Close'
            elif self.state == 'Close':
                self.cursor_rect.midtop = (self.quitx + self.offset, self.quity)
                self.state = 'MM'
            elif self.state == 'MM':
                self.cursor_rect.midtop = (self.creditsx + self.offset, self.creditsy)
                self.state = 'Finalise'
            elif self.state == 'Finalise':
                self.cursor_rect.midtop = (self.helpx + self.offset, self.helpy)
                self.state = 'Again'
            elif self.state == 'Again':
                self.cursor_rect.midtop = (self.deinstx + self.offset, self.deinsty)
                self.state = 'APII'

    def check_input(self):
        self.move_cursor()
        if self.game.START_KEY:
            if self.state == 'Start':
                if self.game.connected:
                    if not os.name == 'nt': # Windows Dev
                        p = subprocess.Popen(['sh', './Scripts/installMagicMirror.sh'])
                    self.game.playing = True
            elif self.state == 'Help':
                self.game.curr_menu = self.game.help
            elif self.state == 'Credits':
                self.game.curr_menu = self.game.credits
            elif self.state == 'Quit':
                self.game.running = False
            elif self.state == 'Deinst':
                if self.game.deinstall:
                    subprocess.call(['sh', './Scripts/deinstallMM.sh'])
                    root = INUIT.Tk()
                    root.withdraw()
                    messagebox.showinfo('Deinstallation', 'Deinstallation Complete!')
                    root.destroy()
            elif self.state == 'Close':
                self.game.running = False

            elif self.state == 'APII':
              self.game.deinstall = True
              if self.game.ownnews or self.game.owncalendar or self.game.ownweather or self.game.ownencouragement:

                #Code für Popups von https://www.python-kurs.eu/tkinter_entry_widgets.php
                global insert_list
                insert_list = []
                for i in range(0,7+ self.game.anzahlEncourage):
                 insert_list.append(0)

                master = INUIT.Tk()
                master.title("Insert Data")
                master.focus_force()

                ico = Image.open('./Images/Inuit_Logo.png')
                photo = ImageTk.PhotoImage(ico)
                master.wm_iconphoto(False, photo)

                reihe = 0
                if not self.game.ownnews and not self.game.owncalendar and not self.game.ownweather and self.game.ownencouragement:
                    w = 315
                else:
                    w = 390
                h = 50
                ws = master.winfo_screenwidth()
                hs = master.winfo_screenheight()
                x = (ws / 2) - (w / 2)
                y = (hs / 2) - (h + 150)

                if self.game.ownnews:
                    h = h +70
                if self.game.owncalendar:
                    h = h + 70
                if self.game.ownweather:
                    h = h + 70
                if self.game.ownencouragement:
                    h = h + (self.game.anzahlEncourage * 24)
                master.geometry('%dx%d+%d+%d' % (w, h, x+20, y))



                if self.game.ownnews:

                    Label(master, text="News Agency").grid(row=reihe)
                    reihe = reihe + 1
                    Label(master, text="RSS-Feed-Link").grid(row=reihe)

                    link1 = Label(master, text="RSS-Feed", fg="blue", cursor="hand2")
                    link1.grid(row=reihe, column=2)
                    link1.bind("<Button-1>", lambda e: callback("https://de.wikipedia.org/wiki/RSS_(Web-Feed)"))

                    reihe = reihe + 1
                    Label(master, text="_____________").grid(row=reihe)
                    reihe = reihe + 1

                    e1 = Entry(master)
                    e2 = Entry(master)
                    e1.grid(row=reihe-3, column=1)
                    e2.grid(row=reihe-2, column=1)

                    insert_list[0] = e1
                    insert_list[1] = e2


                if self.game.owncalendar:
                    Label(master, text="Calendar Name").grid(row=reihe)
                    reihe = reihe + 1
                    Label(master, text="Webaddress").grid(row=reihe)

                    link1 = Label(master, text="Webcal", fg="blue", cursor="hand2")
                    link1.grid(row=reihe, column=2)
                    link1.bind("<Button-1>", lambda e: callback("https://www.sieda.com/dienstplan/hilfe/internetkalender-synchronisieren.php"))

                    reihe = reihe + 1
                    Label(master, text="_____________").grid(row=reihe)
                    reihe = reihe + 1


                    e3 = Entry(master)
                    e4 = Entry(master)
                    e3.grid(row=reihe-3, column=1)
                    e4.grid(row=reihe-2, column=1)
                    insert_list[2] = e3
                    insert_list[3] = e4

                if self.game.ownweather:
                    Label(master, text="Weather Location").grid(row=reihe)
                    reihe = reihe + 1
                    Label(master, text="Citycode").grid(row=reihe)
                    link1 = Label(master, text="Citycode", fg="blue", cursor="hand2")
                    link1.grid(row=reihe, column=2)
                    link1.bind("<Button-1>", lambda e: callback("http://bulk.openweathermap.org/sample/city.list.json.gz"))
                    reihe = reihe + 1
                    Label(master, text="API-Key").grid(row=reihe)
                    link2 = Label(master, text="API-Key", fg="blue", cursor="hand2")
                    link2.grid(row=reihe, column=2)
                    link2.bind("<Button-1>", lambda e: callback("https://openweathermap.org/price"))
                    reihe = reihe + 1
                    Label(master, text="_____________").grid(row=reihe)
                    reihe = reihe + 1

                    e5 = Entry(master)
                    e6 = Entry(master)
                    e7 = Entry(master)
                    e5.grid(row=reihe-4, column=1)
                    e6.grid(row=reihe-3, column=1)
                    e7.grid(row=reihe-2, column=1)
                    insert_list[4] = e5
                    insert_list[5] = e6
                    insert_list[6] = e7

                if self.game.ownencouragement and not self.game.anzahlEncourage == 0:
                    Label(master, text="Encouragement").grid(row=reihe)
                    reihe = reihe + 1

                    en = [0] * self.game.anzahlEncourage

                    for x in range(0,self.game.anzahlEncourage):
                        en[x] = Entry(master)
                        en[x].grid(row=reihe-1, column=1)
                        insert_list[x+7] = en[x]
                        reihe = reihe + 1

                INUIT.Button(master, text='Exit', command=(lambda: ExitPopup(master))).grid(row=reihe, column=0, sticky=W, pady=4)
                INUIT.Button(master, text='Insert', command=(lambda: save_input(self,insert_list,master))).grid(row=reihe, column=1, sticky=W, pady=4)

                master.mainloop()
            elif self.state == 'Again':
                self.game.gameover = False
                self.game.hebelaktiviert = False
                self.game.hebel2aktiviert = False
                self.game.hebel3aktiviert = False
                self.game.hebel4aktiviert = False
                self.game.speaker2 = False
                self.game.facing = ""
                self.game.level = False
                self.game.xp = 20
                self.game.yp = 430
                self.game.restartPC = False
                self.game.shortcut = False
                self.game.autostartMM = False
                self.game.startMM = False
                self.game.ownweather = False
                self.game.owncalendar = False
                self.game.ownencouragement = False
                self.game.ownnews = False
                self.game.validinput = False
                self.game.anzahlEncourage = 0
                self.game.playing = True
            elif self.state == 'Finalise':
              if self.game.validinput:

                if (self.game.ownweather or self.game.ownnews or self.game.ownencouragement or self.game.owncalendar):

                    copyfile("/home/pi/MagicMirror/config/config.js", "./Config/config.js")

                    if self.game.ownnews:
                        #insert_list[0] Newsagency
                        #insert_list[1] RSS-Feed
                        changeline(97,"title: \"" + insert_list[0] + "\"" + ",\n")
                        changeline(98,"url: \"" + insert_list[1] + "\"" + "\n")

                    if self.game.owncalendar:
                        #insert_list[2] Name
                        #insert_list[3] Wabcal
                        changeline(54,"header: \"" + insert_list[2] + "\"" + ",\n")
                        changeline(60,"url: \"" + insert_list[3] + "\" }" + "\n")

                    if self.game.ownweather:
                        #insert_list[4] cityname
                        #insert_list[5] citycode
                        #insert_list[6] API-Key
                        changeline(74,"location: \"" + insert_list[4] + "\"" + ",\n")
                        changeline(75,"locationID: \"" + insert_list[5] + "\"" + ",\n")
                        changeline(76,"apiKey: \"" + insert_list[6] + "\"" + "\n")

                        changeline(86,"location: \"" + insert_list[4] + "\"" + ",\n")
                        changeline(87,"locationID: \"" + insert_list[5] + "\"" + ",\n")
                        changeline(88,"apiKey: \"" + insert_list[6] + "\"" + "\n")

                    if self.game.ownencouragement:
                        changeline(66,"position: \"" + "lower_third" + "\"" + "\n,\n config:{\ncompliments:{\nanytime:[\n\n")

                        count = 0
                        for i in range(7, len(insert_list)):
                            if i == len(insert_list)-1:
                             changeline(71+count,"\"" + insert_list[i] + "\"\n]\n}\n}\n},\n")
                             count = count + 1
                            else:
                             changeline(71 + count, "\"" + insert_list[i] + "\",\n")
                             count = count + 1

                    subprocess.call(['sh', './Scripts/copycfg.sh'])


                if self.game.shortcut:
                    subprocess.call(['sh', './Scripts/createShortcut.sh'])
                if self.game.autostartMM:
                    subprocess.call(['sh', './Scripts/autostart.sh'])
                if self.game.startMM:
                    subprocess.call(['sh', './Scripts/MagicMirror.sh'])
                if self.game.restartPC:
                    subprocess.call(['sh', './Scripts/restart.sh'])

                sys.exit(0)

            elif self.state == 'MM':
                self.game.gameover = False
                self.game.hebelaktiviert = False
                self.game.hebel2aktiviert = False
                self.game.hebel3aktiviert = False
                self.game.hebel4aktiviert = False
                self.game.speaker2 = False
                self.game.facing = ""
                self.game.level = False
                self.game.xp = 20
                self.game.yp = 430
                self.game.restartPC = False
                self.game.shortcut = False
                self.game.autostartMM = False
                self.game.startMM = False
                self.game.ownweather = False
                self.game.owncalendar = False
                self.game.ownencouragement = False
                self.game.ownnews = False
                self.game.validinput = False
                self.game.anzahlEncourage = 0

            self.run_display = False




class HelpMenu(Menu):

    def __init__(self, game):
        Menu.__init__(self, game)
        self.backgroundMain = pygame.image.load("./Images/bgmain_free.jpg")
        self.arrowpng = pygame.image.load("./Images/arrowpng.png")
        self.mauszeiger = pygame.image.load("./Images/mauszeiger.png")
        self.tasta = pygame.image.load("./Images/tasta.png")

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.game.check_events()
            if self.game.START_KEY or self.game.BACK_KEY:
                self.game.curr_menu = self.game.main_menu
                self.run_display = False
            self.game.display.fill(self.game.BLACK)
            self.game.display.blit(self.backgroundMain, (0, 0))
            self.game.draw_text('Help', 30, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 6 -40)
            self.game.draw_text('To install your Smart Mirror an internet connection is required.', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 6 + 20)
            self.game.draw_text('Collect ingame items to install and configure your Smart Mirror.', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 6 + 45)
            self.game.draw_text('You have 2 minutes to gather your Configuration items. If you don\'t', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 6 + 70)
            self.game.draw_text('choose any items, the standard configuration will be applied.', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 6 + 95)


            x = 130
            y = 230

            self.game.draw_text('Game Controls', 20, x, y)
            self.game.display.blit (self.arrowpng, (x-100, y+10))

            x = 140
            y = 380
            self.game.draw_text('Inserting Text', 20, x, y)
            self.game.display.blit (self.mauszeiger, (x+30, y))
            self.game.display.blit (self.tasta, (x-130, y+10))


            self.game.draw_text('Game Icons', 20, 700, 230)

            x = 330
            y = 280
            self.game.display.blit (self.game.wolke, (x, y))
            self.game.display.blit (self.game.newspaper, (x, y+60))
            self.game.display.blit (self.game.speaker, (x, y+120))
            self.game.display.blit (self.game.kalender, (x, y+180))

            self.game.draw_text('= Own Weather', 15, x+170, y+20)
            self.game.draw_text('= Own News', 15, x+150, y+80)
            self.game.draw_text('= Own Encouragement', 15, x+218, y+140)
            self.game.draw_text('= Own Calender', 15, x+185, y+200)


            x = 690
            y = 400
            self.game.display.blit (self.game.verk, (x, y))
            self.game.display.blit (self.game.auto, (x, y-60))
            self.game.display.blit (self.game.restart, (x, y-120))
            self.game.display.blit (self.game.start, (x, y+60))

            self.game.draw_text('= Desktop Shortcut', 15, x+190, y+20)
            self.game.draw_text('= Restart Device', 15, x+175, y-100)
            self.game.draw_text('= Autostart Mirror', 15, x+190, y-40)
            self.game.draw_text('= Start Mirror', 15, x+160, y+80)


            #Bild arrows + enter + maus

            self.blit_screen()




class CreditsMenu(Menu):

    def __init__(self, game):
        Menu.__init__(self, game)
        self.backgroundMain = pygame.image.load("./Images/bgmain.jpg")

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.game.check_events()
            if self.game.START_KEY or self.game.BACK_KEY:
                self.game.curr_menu = self.game.main_menu
                self.run_display = False
            self.game.display.fill(self.game.BLACK)
            self.game.display.blit(self.backgroundMain, (0, 0))
            self.game.draw_text('Credits', 20, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 - 20)
            self.game.draw_text('Made by David Morrow', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 + 30)
            self.game.draw_text('Menu by Christian Duenas @ Youtube', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 + 50)
            self.game.draw_text('Game inspired by Clear Code @ Youtube', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 + 70)
            self.blit_screen()